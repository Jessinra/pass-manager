# User

Last update : 10 March 2020

## Register new user

<details>

### Request

POST `/user`

```JSON
{
    "userDetails": {
        "username": "jest2",
        "email": "jest2@jest",
        "password": "jest",
        "phoneNumber": "+621234567890"
    }
}
```

### Response 200

```JSON
{
    "updatedAt": "2020-03-10T02:03:19.380Z",
    "phoneNumber": "+621234567890",
    "email": "jest2@jest",
    "username": "jest2",
    "id": "5e66f56708813b000725f951",
    "createdAt": "2020-03-10T02:03:19.380Z"
}
```

</details>

## Update user info

<details>

### Request

PUT `/user`

```JSON
{
    "userDetails": {
        "username": "u_jest2",
        "email": "u_jest2@jest",
        "password": "u_jest"
    }
}
```

### Response 200

```JSON
{
    "updatedAt": "2020-03-10T02:03:19.736Z",
    "phoneNumber": "+628123456789",
    "email": "u_jest2@jest",
    "username": "u_jest2",
    "id": "5e66f56708813b000725f953",
    "createdAt": "2020-03-10T02:03:19.604Z"
}
```

</details>

## Update user config

<details>

### Request

PUT `/user/config`

```JSON
{
    "configDetails": {
        "showPassword": true,
        "allowSoftDelete": true,
        "displayPerPage": 2
    }
}
```

### Response 200

```JSON
{
    "id": "5e66f56708813b000725f956",
    "showPassword": true,
    "allowSoftDelete": true,
    "displayPerPage": 2
}
```

</details>

## Get user's accounts

<details>

### Request

GET `/user/account?page=2`

```JSON
{}
```

### Response 200

```JSON
[
    {
        "id": "5e66f56808813b000725f963",
        "name": "acc_10",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f964",
        "name": "acc_11",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f965",
        "name": "acc_12",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f966",
        "name": "acc_13",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f967",
        "name": "acc_14",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f968",
        "name": "acc_15",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f969",
        "name": "acc_16",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f96a",
        "name": "acc_17",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f96b",
        "name": "acc_18",
        "description": null
    },
    {
        "id": "5e66f56808813b000725f96c",
        "name": "acc_19",
        "description": null
    }
]
```

</details>

## Get user's deleted accounts

<details>

### Request

GET `/user/account/deleted`

```JSON
{}
```

### Response 200

```JSON
[
    {
        "id": "5e66f56808813b000725f96f",
        "name": "acc_0",
        "description": null,
        "deletedAt": null
    },
    {
        "id": "5e66f56808813b000725f970",
        "name": "acc_1",
        "description": null,
        "deletedAt": null
    }
]
```

</details>

## Check username availability

<details>

### Request

GET `/user/availability?username=checkAvailabilityHandler`

```JSON
{}
```

### Response 200

```JSON
{
    "username": "checkAvailabilityHandler",
    "isAvailable": false
}
```

</details>
