# Account

last edit : 10 March 2020

## Create new account

<details>

### Request

POST `/account`

```JSON
{
    "accountDetails": {
        "name": "jest account",
        "description": "random account",
        "usernameOrEmail": "jest",
        "password": "jest"
    },
    "secret": "1234"
}
```

### Response 200

```JSON
{
    "deletedAt": null,
    "name": "jest account",
    "updatedAt": "2020-03-10T08:55:09.964Z",
    "usernameOrEmail": "U2FsdGVkX1+tqmTochwIfcfyRZIoRz+U5dSvBd1ss6w=",
    "description": "random account",
    "id": "5e6755ed08813b000725fb68",
    "createdAt": "2020-03-10T08:55:09.964Z",
    "isActive": true,
    "password": "U2FsdGVkX1+GhIz9NulF4WDe3OqDsm02unMgR9McvGs="
}
```

</details>

## Update account info

<details>

### Request

PUT `/account/:accountId`

```JSON
{
    "accountDetails": {
        "name": "mocca account",
        "description": "sdf account",
        "usernameOrEmail": "mocca",
        "password": "mocca"
    },
    "secret": "1234"
}
```

### Response 200

```JSON
{
    "deletedAt": null,
    "name": "mocca account",
    "updatedAt": "2020-03-10T08:55:10.484Z",
    "usernameOrEmail": "U2FsdGVkX19wyP33tvPFiRxrWW6xRFDEe3HJEEC2gpg=",
    "description": "sdf account",
    "id": "5e6755ee08813b000725fb69",
    "createdAt": "2020-03-10T08:55:10.022Z",
    "isActive": true,
    "password": "U2FsdGVkX1+/DySu212kgEhkIX1YhgKfHKm6+AQ5mvc="
}
```

</details>

## Delete account

<details>

### Request

DELETE `/account/:accountId/delete`

```JSON
{}
```

### Response 200

```JSON
{
    "deletedAt": "2020-03-10T08:52:39.311Z",
    "name": "jestAccount",
    "updatedAt": "2020-03-10T08:52:39.317Z",
    "usernameOrEmail": "username",
    "description": null,
    "id": "5e67555708813b000725fb64",
    "createdAt": "2020-03-10T08:52:39.272Z",
    "isActive": false,
    "password": "pass"
}
```

</details>

## Get account info

<details>

### Request

GET `/account/:accountId`

```JSON
{
    "secret": "1234"
}
```

### Notes

secret will be use to decrypt the username & password

### Response 200

```JSON
{
    "deletedAt": null,
    "name": "jestAccount",
    "updatedAt": "2020-03-10T08:52:39.344Z",
    "usernameOrEmail": "",
    "description": null,
    "id": "5e67555708813b000725fb65",
    "createdAt": "2020-03-10T08:52:39.344Z",
    "isActive": true,
    "password": ""
}
```

</details>
