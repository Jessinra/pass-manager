# Authentication

## Sign in

<details>

### Request

POST `/signin`

```JSON
{
    "username": "authentication_test",
    "password": "password"
}
```

### Response 200

```JSON
{
    "accounts": [
        {
            "id": "5e67031d08813b000725faa6",
            "name": "acc_0",
            "description": null
        },
        {
            "id": "5e67031d08813b000725faa7",
            "name": "acc_1",
            "description": null
        }
    ],
    "jwtToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImF1dGhlbnRpY2F0aW9uX3Rlc3QiLCJlbWFpbCI6ImF1dGhlbnRpY2F0aW9uX3Rlc3RAamVzdCIsImlhdCI6MTU4MzgwOTMwOSwiZXhwIjoxNTg0MDY4NTA5fQ.GaRdSNRf48HRLBLIy8oAECEVlZOVvKCAQqDyZD-UpUc"
}
```

</details>
