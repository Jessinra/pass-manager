# Search

last edit : 10 March 2020

## Search (git)

<details>

### Request

GET `/search/account?query=git&first=5&skip=0`

```JSON
{}
```

### Response 200

```JSON
[
    {
        "name": "Github",
        "usernameOrEmail": "username",
        "description": null,
        "id": "5e679cb308813b000725fc30",
        "createdAt": "2020-03-10T13:57:07.654Z"
    },
    {
        "name": "Gitlab",
        "usernameOrEmail": "username",
        "description": null,
        "id": "5e679cb308813b000725fc31",
        "createdAt": "2020-03-10T13:57:07.655Z"
    }
]
```

</details>
