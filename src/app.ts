import Koa = require("koa");
import KoaRouter = require("koa-router");
const cors = require('@koa/cors');

import logger = require("koa-logger");
import bodyParser = require("koa-bodyparser");
import { Scheduler } from "./worker/scheduler";

import { errorHandler } from "./middleware/error";
import { authMiddleware } from "./middleware/authMiddleware";

import authenticationHandler = require("./handler/authentication");
import userHandler = require("./handler/user");
import accountHandler = require("./handler/account");
import accountHandlerSearch = require("./handler/accountSearch");

const app = new Koa();
const router = new KoaRouter();
const scheduler = new Scheduler();

app.use(cors());
app.use(bodyParser());
app.use(logger());
app.use(errorHandler);

app.use(router.routes());
app.use(router.allowedMethods());

// =========== Public / without JWT ===========
router.post(`/user`, userHandler.registerUserHandler);
router.post(`/signin`, authenticationHandler.signInHandler);

// =========== Authorized / with JWT ===========
router.put(`/user/`, authMiddleware, userHandler.updateUserHandler);
router.put(`/user/config`, authMiddleware, userHandler.updateUserConfigHandler);
router.get(`/user/account`, authMiddleware, userHandler.getUserAccountsHandler);
router.get(
  `/user/account/deleted`,
  authMiddleware,
  userHandler.getDeletedUserAccountsHandler
);

router.post(`/account`, authMiddleware, accountHandler.createAccountHandler);
router.get(
  `/account/:accountId`,
  authMiddleware,
  accountHandler.showAccountInfoHandler
);
router.put(
  `/account/:accountId`,
  authMiddleware,
  accountHandler.updateAccountHandler
);
router.delete(
  `/account/:accountId/delete`,
  authMiddleware,
  accountHandler.deleteAccountHandler
);

router.get(
  `search/account`,
  authMiddleware,
  accountHandlerSearch.searchAccountHandler
);

// =========== System use ===========
router.get(`/user/availability`, userHandler.checkAvailabilityHandler);

app.listen(3000, () => {
  console.log(" [*] Xendria listening on port 3000!");
});
