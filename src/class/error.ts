export class ServiceError {

	public error: Error;
	public code: string;
	public errorType: string;
	public errorMessage: string;

	protected constructor(code: string, errorType: string, message?: string) {

		if (!message) {
			message = "No specific information"
		}

		this.error = Error(message);
		this.errorMessage = message
		this.code = code;
		this.errorType = errorType;
	}
}

export class IncorrectUsernameOrPassword extends ServiceError {
	public constructor(message?: string) {
		super("E0001", "INCORRECT_USERNAME_OR_PASSWORD", message)
	}
}

export class FailedCreateUser extends ServiceError {
	public constructor(message?: string) {
		super("E0001", "FAILED_CREATE_USER", message)
	}
}

export class FailedCreateAccount extends ServiceError {
	public constructor(message?: string) {
		super("E0011", "FAILED_CREATE_ACCOUNT", message)
	}
}

// ==================== General ====================
export class ResourceNotFound extends ServiceError {
	public constructor(message?: string) {
		super("E9905", "RESOURCE_NOT_FOUND", message)
	}
}

export class InvalidId extends ServiceError {
	public constructor(message?: string) {
		super("E0001", "INVALID_ID", message)
	}
}

export class FailedCheckAuthorization extends ServiceError {
	public constructor(message?: string) {
		super("E0002", "FAILED_TO_CHECK_AUTHORIZATION", message)
	}
}

export class UserNotAuthorized extends ServiceError {
	public constructor(message?: string) {
		super("E0003", "USER_NOT_AUTHORIZED", message)
	}
}

export class InvalidJWT extends ServiceError {
	public constructor(message?: string) {
		super("E0004", "INVALID_JWT", message)
	}
}

export class FailedToAuthenticateUser extends ServiceError {
	public constructor(message?: string) {
		super("E0005", "FAILED_AUTHENTICATE_USER", message)
	}
}

export class FailedToSearch extends ServiceError {
	public constructor(message?: string) {
		super("E0005", "FAILED_TO_SEARCH", message)
	}
}

export class UnhandledError extends ServiceError {
	public constructor(message?: string) {
		super("E0099", "UNHANDLED_EXCEPTION", message)
	}
}