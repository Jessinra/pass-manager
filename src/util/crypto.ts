
import bcrypt = require('bcryptjs');
import crypto = require('crypto-js')
import pbkdf2 = require('pbkdf2')

require('dotenv').config();


export function hashPassword(password: string) {
    return bcrypt.hashSync(password, 10)
}

export function isCorrectPassword(password: string, hashPassword: string) {
    return bcrypt.compareSync(password, hashPassword)
}

export function encrypt(plainText: string, secret: string) {
    const key = generateKey(secret)
    return crypto.AES.encrypt(plainText, key).toString();
}

export function decrypt(cipherText: string, secret: string) {
    const key = generateKey(secret)
    const bytes = crypto.AES.decrypt(cipherText, key);
    return bytes.toString(crypto.enc.Utf8);
}

function generateKey(secret: string) {
    return pbkdf2.pbkdf2Sync(secret, process.env.CuSO4, 50000, 256, 'sha512').toString('hex');
}