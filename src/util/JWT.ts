
import jwt = require('jsonwebtoken');
import serviceError = require("../class/error")
import { IActiveUser } from '../interface/activeUser';

require('dotenv').config();


export async function signJWT(payload: any) {
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '3 days' });
}

export async function verifyJWT(token: string): Promise<IActiveUser> {

    try {
        const payload: any = jwt.verify(token, process.env.JWT_SECRET)
        const activeUser: IActiveUser = {
            username: payload.username,
            email: payload.email,
            iat: payload.iat,
        }

        return activeUser
    }
    catch (err) {
        throw new serviceError.InvalidJWT(err.message)
    }
}

export async function refreshRequired(jwtCreatedAt: number) {
    const now = new Date().getTime() / 1000
    return (now - jwtCreatedAt) >= 3600 // 1 hour is 'old'
}

export async function refreshJWT(payload) {
    delete payload.iat
    delete payload.exp
    return await signJWT(payload)
}