
import { IPagination } from "../interface/pagination";
import { IActiveUser } from "../interface/activeUser";
import { IUserDetails } from "../model/user";
import { IConfigDetails } from "../model/userConfig";
import User = require("../model/user")
import UserConfig = require("../model/userConfig")


export async function registerUser(userDetails: IUserDetails) {
    return await User.createUser(userDetails)
}

export async function updateUser(userDetails: IUserDetails, activeUser: IActiveUser) {
    return await User.updateUser(activeUser.username, userDetails)
}

export async function updateUserConfig(configDetails: IConfigDetails, activeUser: IActiveUser) {
    return await UserConfig.updateConfig(activeUser.username, configDetails)
}

export async function getUserAccounts(activeUser: IActiveUser, page: number) {

    const config = await User.getConfig(activeUser.username)
    const pagination: IPagination = {
        first: config.displayPerPage,
        skip: config.displayPerPage * (page - 1)
    }

    const accounts = await User.getUserAccount(activeUser.username, pagination)
    return accounts
}

export async function getDeletedUserAccounts(activeUser: IActiveUser) {
    return await User.getDeletedUserAccount(activeUser.username)
}

export async function checkAvailability(username: string) {
    return (!(await User.isUserExist(username)))
}