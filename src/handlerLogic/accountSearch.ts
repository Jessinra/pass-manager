
import _ = require("underscore")

import { IActiveUser } from "../interface/activeUser";
import AccountSearch = require("../model/accountSearch")


export interface ISearchDetails {
    query: string,
    first?: number,
    skip?: number,
}

export async function search(details: ISearchDetails, activeUser: IActiveUser) {

    const keywords = preprocessQuery(details.query);

    let foundIds = await findRelevantIds(keywords, activeUser);
    foundIds = sortByFrequencyAndRemoveDuplicates(foundIds);
    foundIds = sliceArray(foundIds, details.first, details.skip);

    const foundInfos = await AccountSearch.getBatchInfo(foundIds);
    return foundInfos;
}

function preprocessQuery(query: string) {
    return query.toLowerCase().split("%20").join(" ").split(" ");
}

async function findRelevantIds(keywords: string[], activeUser: IActiveUser) {

    let foundIds: string[] = [];
    await Promise.all(keywords.map(async (keyword) => {
        const res = await AccountSearch.findAccountByKeyword(keyword, activeUser);
        foundIds = foundIds.concat(res);
    }));

    return foundIds;
}


function sortByFrequencyAndRemoveDuplicates(array: any[]) {

    let frequency = _.countBy(array);
    let uniques = _.keys(frequency)

    // sort the uniques array in descending order by frequency
    return uniques.sort((a, b) => {
        return frequency[b] - frequency[a];
    });
}

function sliceArray(arr: any[], first: number, skip: number) {
    return arr.slice(skip, skip + first);
}

export async function syncSearchIdx(accountId: string) {
    const searchIdx = await AccountSearch.getSearchIdx(accountId)
    await AccountSearch.updateSearchIdx(accountId, searchIdx)
}