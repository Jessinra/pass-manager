
import { IActiveUser } from "../interface/activeUser";
import { encrypt, decrypt } from "../util/crypto"
import ServiceError = require("../class/error")

import { IAccountDetails } from "../model/account";
import User = require("../model/user")
import Account = require("../model/account")
import AccountSearchLogic = require("../handlerLogic/accountSearch")


export async function createAccount(accountDetails: IAccountDetails, secret: string, activeUser: IActiveUser) {

    if (!(accountDetails.password && secret)) {
        throw new ServiceError.FailedCreateAccount("Missing password / secret !")
    }

    accountDetails.password = encrypt(accountDetails.password, secret)

    const newAccount = await Account.createAccount(accountDetails)
    await User.addAccount(activeUser.username, newAccount.id)
    await AccountSearchLogic.syncSearchIdx(newAccount.id)

    return newAccount
}

export async function updateAccount(accountId: string, accountDetails: IAccountDetails, secret: string) {

    if (accountDetails.password) {
        accountDetails.password = encrypt(accountDetails.password, secret)
    }

    await AccountSearchLogic.syncSearchIdx(accountId)
    const updatedAccount = await Account.updateAccount(accountId, accountDetails)

    return updatedAccount
}

export async function deleteAccount(accountId: string, activeUser: IActiveUser) {

    const config = await User.getConfig(activeUser.username)
    if (config.allowSoftDelete) {
        return await softDeleteAccount(accountId)
    }
    else {
        return await permanentDeleteAccount(accountId)
    }
}

export async function showAccountInfo(accountId: string, secret: string) {

    const account = await Account.getAccount(accountId)
    account.password = decrypt(account.password, secret)

    return account
}

async function permanentDeleteAccount(accountId: string) {
    return await Account.deleteAccount(accountId)
}

async function softDeleteAccount(accountId: string) {
    const currentTime = new Date(new Date().getTime()).toISOString()
    return await Account.updateAccount(accountId, {
        isActive: false,
        deletedAt: currentTime,
    })
}

export async function authorizeUser(accountId: string, activeUser: IActiveUser) {

    if (!(isUserLoggedIn(activeUser))) {
        throw new ServiceError.FailedToAuthenticateUser()
    }

    if (!(await Account.isOwner(accountId, activeUser.username))) {
        throw new ServiceError.UserNotAuthorized()
    }
}

export function authenticateUser(activeUser: IActiveUser) {

    if (!(isUserLoggedIn(activeUser))) {
        throw new ServiceError.FailedToAuthenticateUser()
    }
}

function isUserLoggedIn(activeUser: IActiveUser) {
    return activeUser.username != null && activeUser.username != "UnauthorizedUser";
}