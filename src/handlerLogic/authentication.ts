
import { User } from "../../src/database/generated/prisma-client"
import { IActiveUser } from "../interface/activeUser"
import { isCorrectPassword } from '../util/crypto';
import JWT = require("../util/JWT")

import serviceError = require("../class/error")
import UserModel = require("../model/user")


export async function signIn(username: string, password: string): Promise<{
    activeUser: IActiveUser,
    jwtToken: string
}> {

    const user = await authenticate(username, password)
    const activeUser = constructActiveUser(user)
    const jwtToken = await JWT.signJWT(activeUser)

    return { activeUser, jwtToken };


    function constructActiveUser(user: User): IActiveUser {
        return {
            username: user.username,
            email: user.email,
        }
    }
}

async function authenticate(username: string, password: string) {

    const user = await UserModel.getUser(username)
    if (!(isCorrectPassword(password, user.password))) {
        throw new serviceError.IncorrectUsernameOrPassword("Invalid username / password")
    }

    return user
}