
import { prisma } from "../database/generated/prisma-client";
import { hashPassword } from "../util/crypto";


var mockUser;
var mockAccount;


export async function createUser(username: string = "jest", email: string = "jest@jest") {
    mockUser = await prisma.upsertUser({
        where: {
            username: username,
        },
        create: {
            username: username,
            password: hashPassword("password"),
            email: email,
            config: { create: {} },
            phoneNumber: "+628123456789",
        },
        update: {}
    })
    return mockUser
}

export async function deleteUser(user: { id?: string, username?: string }) {

    let userInfo: any = await prisma.user(user).$fragment(`{
        username
        config {
            id
        }
        account {
            id
        }
    }`)

    if (userInfo.config) {
        await prisma.deleteConfig({ id: userInfo.config?.id })
    }

    await prisma.deleteManyAccounts({ id_in: userInfo.account?.map(({ id }) => id) })
    await prisma.deleteUser({ username: userInfo.username })
}

export async function resetUser() {
    await prisma.deleteManyConfigs()
    await prisma.deleteManyUsers()
}

export async function createAccount(name: string = "jestAccount", isActive: boolean = true) {
    mockAccount = await prisma.createAccount({
        name: name,
        usernameOrEmail: "username",
        password: "pass",
        isActive: isActive,
        user: { connect: { username: mockUser.username } },
        _searchIdx: `${name}`.toLowerCase()
    })
    return mockAccount
}

export async function deleteAccount(id: string) {
    await prisma.deleteAccount({ id })
}

export async function resetAccount() {
    await prisma.deleteManyAccounts()
}