
var LOG_REQUEST_RESPONSE = true

export function setLogger(state: boolean) {
    LOG_REQUEST_RESPONSE = state
}

export function logRequestResponse(name: string, mockCtx: any, method: string = "METHOD", endpoint: string = "ENDPOINT", notes: string = "") {
    logRequest(name, mockCtx, method, endpoint, notes)
    logResponse(name, mockCtx)
}

export function logRequest(name: string, mockCtx: any, method: string = "METHOD", endpoint: string = "ENDPOINT", notes: string = undefined) {

    const request = Object.assign({}, mockCtx.request.body)
    const query = Object.assign({}, mockCtx.request.query ? mockCtx.request.query : {})
    const queryString = Object.keys(query).map(key => key + '=' + query[key]).join('&');

    if (queryString != "") {
        endpoint = `${endpoint}?${queryString}`
    }

    request.activeUser = undefined

    if (LOG_REQUEST_RESPONSE) {
        console.log(
            `
## ${name}

<details>

### Request

${method} \`${endpoint}\`

\`\`\`JSON
${JSON.stringify(request, null, 4)}
\`\`\`
${(notes) ? getFormattedNotes(notes) : ""}
`
        )
    }
}

function getFormattedNotes(notes: string) {
    return `
### Notes

${notes}
`
}

export function logResponse(name: string, mockCtx: any) {

    if (LOG_REQUEST_RESPONSE) {
        console.log(
            `
### Response 200

\`\`\`JSON
${JSON.stringify(mockCtx.body, null, 4)}
\`\`\`

</details>

`
        )
    }
}