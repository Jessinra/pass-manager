import { prisma } from "../database/generated/prisma-client";

import testHelper = require('../test/helper')
import testLogger = require('../test/logger')

import { dbCleanerWorker } from "../worker/dbCleanerWorker"

testLogger.setLogger(false)

var mockUser;


beforeAll(async () => {
    mockUser = await testHelper.createUser("dbCleanerWorker.test", "dbCleanerWorker.test@jest")
});

afterAll(async () => {
    await testHelper.deleteUser({ username: "dbCleanerWorker.test" })
});


describe("Feeds Cleaner Worker", () => {


    beforeAll(async () => {
        for (let i = 0; i < 10; i++) {

            const isOdd = (i % 2 != 0)

            await prisma.createAccount({
                name: "jest account",
                usernameOrEmail: "username",
                password: "pass",
                isActive: isOdd,
                deletedAt: (isOdd) ? null : "1999-01-01T08:55:09.964Z",
                user: { connect: { username: mockUser.username } }
            })
        }
    })

    afterAll(async () => {
        await prisma.deleteManyAccounts({
            user: {
                id: mockUser.id
            }
        })
    })

    test("normal execution", async () => {

        await dbCleanerWorker.execute()

        let accounts = await prisma.user({
            username: mockUser.username
        }).account()

        expect(accounts).toHaveLength(5)
    })
})