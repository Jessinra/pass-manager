
import { dbCleanerWorker } from "./dbCleanerWorker"


export class Scheduler {

    public constructor() {

        // Choose time from prime number : https://en.wikipedia.org/wiki/List_of_prime_numbers 

        this.runTaskHourly(async () => {
            await dbCleanerWorker.runWorker()
        }, 0.331)

        console.log(" [*] Scheduled Worker setup successfully !")
    }

    private runTaskDaily(task: Function, repeatEveryHour: number) {
        setInterval(async () => {
            await this.wrapAndRun(task)
        }, this.dayToSecond(repeatEveryHour));
    }

    private runTaskHourly(task: Function, repeatEveryHour: number) {
        setInterval(async () => {
            await this.wrapAndRun(task)
        }, this.hourToSecond(repeatEveryHour));
    }

    private runTaskMinutely(task: Function, repeatEveryMin: number) {
        setInterval(async () => {
            await this.wrapAndRun(task)
        }, this.minuteToSecond(repeatEveryMin));
    }

    private runTaskSecondly(task: Function, repeatEverySec: number) {
        setInterval(async () => {
            await this.wrapAndRun(task)
        }, this.toSecond(repeatEverySec));
    }

    private dayToSecond(day: number) {
        return this.hourToSecond(24 * day);
    }

    private hourToSecond(hour: number) {
        return this.minuteToSecond(60 * hour);
    }

    private minuteToSecond(minute: number) {
        return minute * 60 * 1000
    }

    private toSecond(second: number) {
        return second * 1000
    }

    private async wrapAndRun(task: Function) {
        try {
            await task()
        } catch (err) {
            logError(err)
        }

        function logError(err: any) {
            console.log("\n\n===========================");
            console.log(new Date().toISOString());
            console.log(err);
            console.log("===========================\n\n");
        }
    }
}