
import { prisma } from "../database/generated/prisma-client"
import { BaseWorker } from "./baseWorker";


class DBCleanerWorker extends BaseWorker {

    async execute(): Promise<void> {

        await prisma.deleteManyAccounts({
            isActive: false,
            deletedAt_lte: lastNDaysDate(3)
        })


        function lastNDaysDate(days: number = 3) {
            const now = new Date().getTime();
            const target = now - (days * 24 * 60 * 60 * 1000)
            return new Date(target).toISOString()
        }
    }

    protected async onFailureCallback(): Promise<void> {
        return null
    }
}


export var dbCleanerWorker = new DBCleanerWorker()
    ._setName("DB_cleaner_worker")