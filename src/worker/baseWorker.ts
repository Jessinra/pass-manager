

export abstract class BaseWorker {

    protected name: string = "Worker Base";

    _setName(name: string) {
        this.name = name
        return this
    }

    protected log() {
        console.log(`> Executing Worker : ${this.name}`)
    }

    protected logError(err: any) {
        console.log("\n\n===========================");
        console.log(`${this.name} Error`);
        console.log("\n")
        console.log(err);
        console.log("===========================\n\n");
    }

    async runWorker() {

        this.log()
        try {
            await this.execute();
        }
        catch (err) {
            this.logError(err)
            await this.onFailureCallback()
        }
    }

    abstract async execute(): Promise<void>;
    /*
        main job to be done by worker
    */

    protected abstract async onFailureCallback(): Promise<void>
    /*
        called upon exception
    */
}