
import { prisma, AccountOrderByInput } from '../../src/database/generated/prisma-client'
import { IPagination } from "../interface/pagination"
import { hashPassword } from '../util/crypto';
import ServiceError = require("../class/error")


export interface IUserDetails {
    username: string,
    email: string,
    password: string,
    phoneNumber: string,
}

export async function createUser(userDetails: IUserDetails) {
    try {
        const user = await prisma.createUser({
            username: userDetails.username,
            email: userDetails.email,
            password: hashPassword(userDetails.password),
            phoneNumber: userDetails.phoneNumber,
            config: { create: {} }
        })
        user.password = undefined
        return user
    }
    catch (err) {
        throw new ServiceError.FailedCreateUser(err.message)
    }
}

export async function updateUser(username: string, userDetails: IUserDetails) {
    try {
        const user = await prisma.updateUser({
            where: {
                username: username,
            },
            data: {
                username: userDetails.username,
                email: userDetails.email,
                password: (userDetails.password) ? hashPassword(userDetails.password) : undefined,
                phoneNumber: userDetails.phoneNumber,
            }
        })
        user.password = undefined
        return user
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function addAccount(username: string, accountId: string) {
    try {
        await prisma.updateUser({
            where: {
                username: username,
            },
            data: {
                account: { connect: { id: accountId } }
            }
        })
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function getUser(username: string) {
    try {
        const user = await prisma.user({
            username: username
        })
        return user
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function getConfig(username: string) {
    try {
        const config = await prisma.user({
            username: username
        }).config()
        return config
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export interface ICompactAccountInfo {
    id: string,
    name: string,
    description: string,
    usernameOrEmail: string
    createdAt: string,
}

export async function getUserAccount(username: string, pagination: IPagination): Promise<ICompactAccountInfo[]> {

    try {
        const accounts: ICompactAccountInfo[] = await prisma.user({
            username: username
        }).account({
            first: pagination.first,
            skip: pagination.skip,
            orderBy: getOrderBy(pagination),
            where: {
                isActive: true
            }
        }).$fragment(`{
            id
            name
            description
            usernameOrEmail
            createdAt
        }`)
        return accounts
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }


    function getOrderBy(pagination: IPagination): AccountOrderByInput {

        const allowedValue = [
            "name_ASC", "name_DESC",
            "description_ASC", "description_DESC",
            "createdAt_ASC", "createdAt_DESC",
            "updatedAt_ASC", "updatedAt_DESC",
        ]

        if (allowedValue.includes(pagination.orderBy)) {
            return pagination.orderBy as AccountOrderByInput
        }
        else {
            return "createdAt_DESC"
        }
    }
}

export interface ICompactDeletedAccountInfo {
    id: string,
    name: string,
    description: string,
    deletedAt: string,
}

export async function getDeletedUserAccount(username: string): Promise<ICompactDeletedAccountInfo[]> {
    try {
        const accounts: ICompactDeletedAccountInfo[] = await prisma.user({
            username: username
        }).account({
            where: {
                isActive: false
            }
        }).$fragment(`{
            id
            name
            description
            deletedAt
        }`)
        return accounts
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function isUserExist(username: string) {
    try {
        const isExist = await prisma.$exists.user({
            username: username
        });
        return isExist;
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}