
import { prisma } from '../../src/database/generated/prisma-client'
import ServiceError = require("../class/error")
import { IActiveUser } from '../interface/activeUser'
import { ICompactAccountInfo } from '../model/user'


// ============================== Search Function ==============================
export async function findAccountByKeyword(keyword: string, activeUser: IActiveUser): Promise<string[]> {

    try {
        const accountIds: { id: string }[] = await prisma.user({
            username: activeUser.username
        }).account({
            where: {
                isActive: true,
                _searchIdx_contains: keyword.toLowerCase()
            },
        }).$fragment(`{
            id
        }`)

        var searchResult = accountIds.map(({ id }) => id)
    }
    catch (err) {
        console.log(new ServiceError.FailedToSearch(err.message))
    }
    finally {
        return searchResult || []
    }
}

export async function getBatchInfo(accountIds: string[]) {
    try {
        const accounts: ICompactAccountInfo[] = await prisma.accounts({
            where: {
                id_in: accountIds
            }
        }).$fragment(`{
            id
            name
            description
            usernameOrEmail
            createdAt
        }`)
        return accounts
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}


// ============================== Search Idx ==============================
export async function updateSearchIdx(accountId: string, searchIdx: string) {
    try {
        await prisma.updateAccount({
            where: {
                id: accountId
            },
            data: {
                _searchIdx: searchIdx
            }
        })
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function getSearchIdx(accountId: string) {
    try {
        const { name, usernameOrEmail, description } = await prisma.account({
            id: accountId,
        }).$fragment(`{
            name
            usernameOrEmail
            description
        }`)

        return `${name} ${usernameOrEmail} ${description}`.toLowerCase()
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

