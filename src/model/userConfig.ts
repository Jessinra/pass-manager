
import { prisma, ConfigUpdateWithoutUserDataInput } from '../../src/database/generated/prisma-client'
import ServiceError = require("../class/error")


export interface IConfigDetails extends ConfigUpdateWithoutUserDataInput {
    // (alias only) intentionally left blank 
}

export async function updateConfig(username: string, configDetails: IConfigDetails) {
    try {
        const config = await prisma.updateUser({
            where: {
                username: username
            },
            data: {
                config: {
                    update: { ...configDetails }
                }
            }
        }).config()
        return config
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}