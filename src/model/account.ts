
import { prisma } from '../../src/database/generated/prisma-client'
import ServiceError = require("../class/error")


export interface IAccountDetails {
    name?: string,
    description?: string,
    usernameOrEmail?: string,
    password?: string,
    isActive?: boolean,
    deletedAt?: string,

    owner?: string,
}

export async function createAccount(accountDetails: IAccountDetails) {
    try {
        const account = await prisma.createAccount({
            name: accountDetails.name,
            description: accountDetails.description,
            usernameOrEmail: accountDetails.usernameOrEmail,
            password: accountDetails.password,
        })
        return account
    }
    catch (err) {
        throw new ServiceError.FailedCreateAccount(err.message)
    }
}

export async function getAccount(accountId: string) {
    try {
        const account = await prisma.account({
            id: accountId,
        })
        return account
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function updateAccount(accountId: string, accountDetails: IAccountDetails) {
    try {
        const account = await prisma.updateAccount({
            where: {
                id: accountId,
            },
            data: {
                name: accountDetails.name,
                description: accountDetails.description,
                usernameOrEmail: accountDetails.usernameOrEmail,
                password: accountDetails.password,
                isActive: accountDetails.isActive,
                deletedAt: accountDetails.deletedAt
            }
        })
        return account
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function deleteAccount(accountId: string) {
    try {
        const account = await prisma.deleteAccount({
            id: accountId,
        })
        return account
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}

export async function isOwner(accountId: string, username: string) {
    try {
        const user = await prisma.account({
            id: accountId,
        }).user()
        return user.username == username
    }
    catch (err) {
        throw new ServiceError.ResourceNotFound(err.message)
    }
}