
import { prisma } from "../database/generated/prisma-client";
import * as searchHandler from "./accountSearch";

import testHelper = require('../test/helper')
import testLogger = require("../test/logger")
import commonError = require("../class/error");
import serviceError = require("../class/error");

testLogger.setLogger(false);

var mockUser;
var mockCtx;


beforeAll(async () => {
    mockUser = await testHelper.createUser("accountSearch_test", "accountSearch_test@jest")
});

afterAll(async () => {
    await testHelper.deleteUser({ username: "accountSearch_test" })
});

beforeEach(() => {
    mockCtx = getTemplateMockCtx();
});

function getTemplateMockCtx() {
    return {
        params: {},
        request: {
            query: {},
            body: {
                activeUser: { username: mockUser.username },
            }
        }
    };
}

describe("searchAccountHandler", () => {

    let accountNames;

    beforeAll(async () => {

        accountNames = [
            "Xendit", "Github", "Gitlab", "Facebook", "Twitter", "Tumblr",
            "Instagram", "Arknights", "Shopee", "Tokopedia", "Bukalapak",
            "Shopee sg", "Gojek", "Grab"
        ]

        await Promise.all(accountNames.map(async (x) => {
            await testHelper.createAccount(x, true)
        }))

    })

    afterAll(async () => {
        await prisma.deleteManyAccounts({
            name_in: accountNames
        })
    })

    test("normal execution", async () => {

        mockCtx.request.query = {
            query: "git",
            first: "5",
            skip: "0"
        }

        await searchHandler.searchAccountHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveLength(2)
        expect(response[0]).toHaveProperty('id')
        expect(response[0]).toHaveProperty('name')
        expect(response[0]).toHaveProperty('description')
        expect(response[0]).toHaveProperty('usernameOrEmail')
        expect(response[0]).toHaveProperty('createdAt')

        testLogger.logRequestResponse(
            "Search (git)", mockCtx,
            "GET", "/search/account"
        )
    })
})