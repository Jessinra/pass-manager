
import { prisma } from "../database/generated/prisma-client";
import * as accountHandler from "./account";

import testHelper = require('../test/helper')
import testLogger = require("../test/logger")
import commonError = require("../class/error");
import serviceError = require("../class/error");

testLogger.setLogger(false);

var mockUser;
var mockCtx;


beforeAll(async () => {
    mockUser = await testHelper.createUser("account_test", "account_test@jest")
});

afterAll(async () => {
    await testHelper.deleteUser({ username: "account_test" })
});

beforeEach(() => {
    mockCtx = getTemplateMockCtx();
});

function getTemplateMockCtx() {
    return {
        params: {},
        request: {
            query: {},
            body: {
                activeUser: { username: mockUser.username },
            }
        }
    };
}

describe("createAccountHandler", () => {

    let accountId: string;

    beforeEach(async () => {
        mockCtx.request.body.accountDetails = {
            name: "jest account",
            description: "random account",
            usernameOrEmail: "jest",
            password: "jest"
        }
        mockCtx.request.body.secret = "1234"
    })

    afterEach(async () => {
        await testHelper.deleteAccount(accountId)
    })

    test("normal execution", async () => {

        testLogger.logRequest(
            "Create new account", mockCtx,
            "POST", "/account"
        )

        await accountHandler.createAccountHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('id')
        accountId = response.id

        expect(response).toHaveProperty('name', "jest account")
        expect(response).toHaveProperty('description', "random account")
        expect(response).toHaveProperty('usernameOrEmail')
        expect(response).toHaveProperty('password')
        expect(response).toHaveProperty('isActive', true)
        expect(response).toHaveProperty('createdAt')

        testLogger.logResponse("", mockCtx)
    })
})

describe("updateAccountHandler", () => {

    let account

    beforeAll(async () => {
        account = await testHelper.createAccount()
    })

    beforeEach(async () => {
        mockCtx.params = { accountId: account.id }
        mockCtx.request.body.accountDetails = {
            name: "mocca account",
            description: "sdf account",
            usernameOrEmail: "mocca",
            password: "mocca"
        }
        mockCtx.request.body.secret = "1234"
    })

    afterAll(async () => {
        await testHelper.deleteAccount(account)
    })

    test("normal execution", async () => {

        testLogger.logRequest(
            "Update account info", mockCtx,
            "PUT", "/account/:accountId"
        )

        await accountHandler.updateAccountHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('name', "mocca account")
        expect(response).toHaveProperty('description', "sdf account")
        expect(response).toHaveProperty('usernameOrEmail')
        expect(response).toHaveProperty('password')
        expect(response).toHaveProperty('isActive', true)
        expect(response).toHaveProperty('createdAt')

        testLogger.logResponse("", mockCtx)
    })
})

describe("deleteAccountHandler", () => {

    let account

    beforeAll(async () => {
        account = await testHelper.createAccount()
    })

    beforeEach(async () => {
        mockCtx.params = { accountId: account.id }
    })

    afterAll(async () => {
        await testHelper.deleteAccount(account)
    })

    test("normal execution", async () => {

        await accountHandler.deleteAccountHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('id')
        expect(response).toHaveProperty('isActive', false)
        expect(response).toHaveProperty('deletedAt')

        testLogger.logRequestResponse(
            "Delete account", mockCtx,
            "DELETE", "/account/:accountId/delete"
        )
    })
})

describe("showAccountInfoHandler", () => {

    let account

    beforeAll(async () => {
        account = await testHelper.createAccount()
    })

    beforeEach(async () => {
        mockCtx.params = { accountId: account.id }
        mockCtx.request.body.secret = "1234"
    })

    afterAll(async () => {
        await testHelper.deleteAccount(account)
    })

    test("normal execution", async () => {

        await accountHandler.showAccountInfoHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('id')
        expect(response).toHaveProperty('name')
        expect(response).toHaveProperty('description')
        expect(response).toHaveProperty('usernameOrEmail')
        expect(response).toHaveProperty('password')
        expect(response).toHaveProperty('isActive', true)
        expect(response).toHaveProperty('createdAt')

        testLogger.logRequestResponse(
            "Get account info", mockCtx,
            "GET", "/account/:accountId",
            "secret will be use to decrypt the username & password"
        )
    })
})