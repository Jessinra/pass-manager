
import { prisma } from "../database/generated/prisma-client";
import * as authHandler from "./authentication";

import testHelper = require('../test/helper')
import testLogger = require("../test/logger")
import commonError = require("../class/error");
import serviceError = require("../class/error");

testLogger.setLogger(false);

var mockUser;
var mockCtx;


beforeAll(async () => {
    mockUser = await testHelper.createUser("authentication_test", "authentication_test@jest")
});

afterAll(async () => {
    await testHelper.deleteUser({ username: "authentication_test" })
});

beforeEach(() => {
    mockCtx = getTemplateMockCtx();
});

function getTemplateMockCtx() {
    return {
        params: {},
        request: {
            query: {},
            body: {}
        }
    };
}

describe("signInHandler", () => {

    let accountIds = [];

    beforeAll(async () => {
        for (let i = 0; i < 2; i++) {
            const account = await testHelper.createAccount(`acc_${i}`)
            accountIds.push(account.id)
        }
    })

    beforeEach(async () => {
        mockCtx.request.body = {
            username: "authentication_test",
            password: "password"
        }
    })

    afterAll(async () => {
        await Promise.all(accountIds.map(async (id) => {
            await testHelper.deleteAccount(id)
        }))
    })

    test("normal execution", async () => {

        await authHandler.signInHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('jwtToken')
        expect(response).toHaveProperty('accounts')

        testLogger.logRequestResponse(
            "Sign in", mockCtx,
            "POST", "/user/signin"
        )
    })
})