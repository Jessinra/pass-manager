
import {
    ISearchDetails,
    search,
} from "../handlerLogic/accountSearch";


export async function searchAccountHandler(ctx, next) {

    const { searchDetails, activeUser } = parseRequest(ctx)
    const searchResult = await search(searchDetails, activeUser)
    ctx.body = formatResponse(searchResult);


    function parseRequest(ctx) {

        const { activeUser } = ctx.request.body
        const { query, first, skip } = ctx.request.query

        const searchDetails: ISearchDetails = {
            query: query,
            first: (first) ? Number(first) : 50,
            skip: (skip) ? Number(skip) : 0,
        }

        return { searchDetails, activeUser }
    }

    function formatResponse(searchResult: any[]) {
        return searchResult
    }
}