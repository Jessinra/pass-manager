
import { ICompactAccountInfo } from "../model/user";
import UserLogic = require("../handlerLogic/user")
import {
    signIn
} from "../handlerLogic/authentication";


export async function signInHandler(ctx, next) {

    const { username, password } = parseRequest(ctx)
    const { activeUser, jwtToken } = await signIn(username, password)
    const accounts = await UserLogic.getUserAccounts(activeUser, 1)
    ctx.body = formatResponse(accounts, jwtToken);


    function parseRequest(ctx) {
        const { username, password } = ctx.request.body
        return { username, password }
    }

    function formatResponse(accounts: ICompactAccountInfo[], jwtToken: string) {
        return {
            accounts,
            jwtToken
        }
    }
}