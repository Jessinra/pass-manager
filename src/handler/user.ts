
import { User, Config } from "../database/generated/prisma-client";
import { ICompactAccountInfo, ICompactDeletedAccountInfo } from "../model/user";
import {
    registerUser,
    updateUser,
    updateUserConfig,
    getUserAccounts,
    getDeletedUserAccounts,
    checkAvailability,
} from "../handlerLogic/user";


export async function registerUserHandler(ctx, next) {

    const { userDetails } = parseRequest(ctx)
    const newUser = await registerUser(userDetails)
    ctx.body = formatResponse(newUser);


    function parseRequest(ctx) {
        const { userDetails } = ctx.request.body
        return { userDetails }
    }

    function formatResponse(user: User) {
        return user
    }
}

export async function updateUserHandler(ctx, next) {

    const { userDetails, activeUser } = parseRequest(ctx)
    const updatedUser = await updateUser(userDetails, activeUser)
    ctx.body = formatResponse(updatedUser);


    function parseRequest(ctx) {
        const { userDetails, activeUser } = ctx.request.body
        return { userDetails, activeUser }
    }

    function formatResponse(user: User) {
        return user
    }
}

export async function updateUserConfigHandler(ctx, next) {

    const { configDetails, activeUser } = parseRequest(ctx)
    const updatedConfig = await updateUserConfig(configDetails, activeUser)
    ctx.body = formatResponse(updatedConfig);


    function parseRequest(ctx) {
        const { configDetails, activeUser } = ctx.request.body
        return { configDetails, activeUser }
    }

    function formatResponse(config: Config) {
        return config
    }
}

export async function getUserAccountsHandler(ctx, next) {

    const { activeUser, page } = parseRequest(ctx)
    const accounts = await getUserAccounts(activeUser, page)
    ctx.body = formatResponse(accounts);


    function parseRequest(ctx) {

        const { activeUser } = ctx.request.body
        const { page } = ctx.request.query

        return {
            activeUser,
            page: (page) ? Number(page) : 1
        }
    }

    function formatResponse(accounts: ICompactAccountInfo[]) {
        return accounts
    }
}

export async function getDeletedUserAccountsHandler(ctx, next) {

    const { activeUser } = parseRequest(ctx)
    const accounts = await getDeletedUserAccounts(activeUser)
    ctx.body = formatResponse(accounts);


    function parseRequest(ctx) {

        const { activeUser } = ctx.request.body
        return { activeUser }
    }

    function formatResponse(accounts: ICompactDeletedAccountInfo[]) {
        return accounts
    }
}

export async function checkAvailabilityHandler(ctx, next) {

    const { username } = parseRequest(ctx)
    const isAvailable = await checkAvailability(username)
    ctx.body = formatResponse(username, isAvailable);


    function parseRequest(ctx) {
        const { username } = ctx.request.query
        return { username }
    }

    function formatResponse(username: string, isAvailable: boolean) {
        return {
            username,
            isAvailable
        }
    }
}