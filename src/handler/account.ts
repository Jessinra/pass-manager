
import { Account } from "../database/generated/prisma-client";
import ServiceError = require("../class/error")
import {
    createAccount,
    updateAccount,
    deleteAccount,
    showAccountInfo,
    authorizeUser,
    authenticateUser,
} from "../handlerLogic/account";


export async function createAccountHandler(ctx, next) {

    const { accountDetails, secret, activeUser } = parseRequest(ctx)
    authenticateUser(activeUser)
    const newAccount = await createAccount(accountDetails, secret, activeUser)
    ctx.body = formatResponse(newAccount);


    function parseRequest(ctx) {
        const { accountDetails, secret, activeUser } = ctx.request.body
        return {
            accountDetails,
            secret: (secret) ? String(secret) : "",
            activeUser
        }
    }

    function formatResponse(account: Account) {
        return account
    }
}

export async function updateAccountHandler(ctx, next) {

    const { accountId, accountDetails, secret, activeUser } = parseRequest(ctx)
    await authorizeUser(accountId, activeUser)
    const updatedAccount = await updateAccount(accountId, accountDetails, secret)
    ctx.body = formatResponse(updatedAccount);


    function parseRequest(ctx) {

        const { accountDetails, secret, activeUser } = ctx.request.body
        const { accountId } = ctx.params

        if (!(accountId)) {
            throw new ServiceError.InvalidId()
        }

        return {
            accountId,
            accountDetails,
            secret: (secret) ? String(secret) : "",
            activeUser
        }
    }

    function formatResponse(account: Account) {
        return account
    }
}

export async function deleteAccountHandler(ctx, next) {

    const { accountId, activeUser } = parseRequest(ctx)
    await authorizeUser(accountId, activeUser)
    const deletedAccount = await deleteAccount(accountId, activeUser)
    ctx.body = formatResponse(deletedAccount);


    function parseRequest(ctx) {

        const { activeUser } = ctx.request.body
        const { accountId } = ctx.params

        if (!(accountId)) {
            throw new ServiceError.InvalidId()
        }

        return { accountId, activeUser }
    }

    function formatResponse(account: Account) {
        return account
    }
}

export async function showAccountInfoHandler(ctx, next) {

    const { accountId, secret, activeUser } = parseRequest(ctx)
    await authorizeUser(accountId, activeUser)
    const accountInfo = await showAccountInfo(accountId, secret)
    ctx.body = formatResponse(accountInfo);


    function parseRequest(ctx) {

        const { activeUser, secret } = ctx.request.body
        const { accountId } = ctx.params

        if (!(accountId)) {
            throw new ServiceError.InvalidId()
        }

        return {
            accountId,
            secret: (secret) ? String(secret) : "",
            activeUser
        }
    }

    function formatResponse(account: Account) {
        return account
    }
}