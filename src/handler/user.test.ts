
import { prisma } from "../database/generated/prisma-client";
import * as userHandler from "./user";

import testHelper = require('../test/helper')
import testLogger = require("../test/logger")
import commonError = require("../class/error");
import serviceError = require("../class/error");

testLogger.setLogger(false);

var mockUser;
var mockCtx;


beforeAll(async () => {
    mockUser = await testHelper.createUser("user_test", "user_test@jest")
});

afterAll(async () => {
    await testHelper.deleteUser({ username: "user_test" })
});

beforeEach(() => {
    mockCtx = getTemplateMockCtx();
});

function getTemplateMockCtx() {
    return {
        params: {},
        request: {
            query: {},
            body: {}
        }
    };
}

describe("registerUserHandler", () => {

    beforeEach(async () => {
        mockCtx.request.body = {
            userDetails: {
                username: "jest2",
                email: "jest2@jest",
                password: "jest",
                phoneNumber: "+621234567890"
            }
        }
    })

    afterEach(async () => {
        await testHelper.deleteUser({ username: "jest2" })
    })

    test("normal execution", async () => {

        await userHandler.registerUserHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('id')
        expect(response).toHaveProperty('username', "jest2")
        expect(response).toHaveProperty('email', "jest2@jest")
        expect(response).toHaveProperty('phoneNumber', "+621234567890")

        testLogger.logRequestResponse(
            "Register new user", mockCtx,
            "POST", "/user"
        )
    })
})

describe("updateUserHandler", () => {

    let user;

    beforeAll(async () => {
        user = await testHelper.createUser("updateUserHandler", "updateUserHandler@jest")
    })

    beforeEach(async () => {
        mockCtx.request.body = {
            activeUser: { username: user.username },
            userDetails: {
                username: "u_jest2",
                email: "u_jest2@jest",
                password: "u_jest",
            }
        }
    })

    afterAll(async () => {
        await testHelper.deleteUser({ id: user.id })
    })

    test("normal execution", async () => {

        await userHandler.updateUserHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('id')
        expect(response).toHaveProperty('username', "u_jest2")
        expect(response).toHaveProperty('email', "u_jest2@jest")
        expect(response).toHaveProperty('phoneNumber')

        testLogger.logRequestResponse(
            "Update user info", mockCtx,
            "PUT", "/user"
        )
    })
})

describe("updateUserConfigHandler", () => {

    let user;

    beforeAll(async () => {
        user = await testHelper.createUser("updateUserConfigHandler", "updateUserConfigHandler@jest")
    })

    beforeEach(async () => {
        mockCtx.request.body = {
            activeUser: { username: user.username },
            configDetails: {
                showPassword: true,
                allowSoftDelete: true,
                displayPerPage: 2,
            }
        }
    })

    afterAll(async () => {
        await testHelper.deleteUser({ id: user.id })
    })

    test("normal execution", async () => {

        await userHandler.updateUserConfigHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('id')
        expect(response).toHaveProperty('showPassword', true)
        expect(response).toHaveProperty('allowSoftDelete', true)
        expect(response).toHaveProperty('displayPerPage', 2)

        testLogger.logRequestResponse(
            "Update user config", mockCtx,
            "PUT", "/user/config"
        )
    })
})

describe("getUserAccountsHandler", () => {

    let user;
    let accountIds = [];

    beforeAll(async () => {
        user = await testHelper.createUser("getUserAccountsHandler", "getUserAccountsHandler@jest")
        for (let i = 0; i < 20; i++) {
            const account = await testHelper.createAccount(`acc_${i}`)
            accountIds.push(account.id)
        }
    })

    beforeEach(async () => {
        mockCtx.request.query = {
            page: "2"
        }
        mockCtx.request.body = {
            activeUser: { username: user.username },
        }
    })

    afterAll(async () => {
        await Promise.all(accountIds.map(async (id) => {
            await testHelper.deleteAccount(id)
        }))
        await testHelper.deleteUser({ id: user.id })
    })

    test("normal execution", async () => {

        await userHandler.getUserAccountsHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveLength(10)
        expect(response[0]).toHaveProperty('id')
        expect(response[0]).toHaveProperty('name')
        expect(response[0]).toHaveProperty('description')

        testLogger.logRequestResponse(
            "Get user's accounts", mockCtx,
            "GET", "/user/account"
        )
    })
})

describe("getDeletedUserAccountsHandler", () => {

    let user;
    let accountIds = [];

    beforeAll(async () => {
        user = await testHelper.createUser("getDeletedUserAccountsHandler", "getDeletedUserAccountsHandler@jest")
        for (let i = 0; i < 2; i++) {
            const account = await testHelper.createAccount(`acc_${i}`, false)
            accountIds.push(account.id)
        }
    })

    beforeEach(async () => {
        mockCtx.request.body = {
            activeUser: { username: user.username },
        }
    })

    afterAll(async () => {
        await Promise.all(accountIds.map(async (id) => {
            await testHelper.deleteAccount(id)
        }))
        await testHelper.deleteUser({ id: user.id })
    })

    test("normal execution", async () => {

        await userHandler.getDeletedUserAccountsHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveLength(2)
        expect(response[0]).toHaveProperty('id')
        expect(response[0]).toHaveProperty('name')
        expect(response[0]).toHaveProperty('description')

        testLogger.logRequestResponse(
            "Get user's deleted accounts", mockCtx,
            "GET", "/user/account/deleted"
        )
    })
})

describe("checkAvailabilityHandler", () => {

    let user;

    beforeAll(async () => {
        user = await testHelper.createUser("checkAvailabilityHandler", "checkAvailabilityHandler@jest")
    })

    beforeEach(async () => {
        mockCtx.request.query = {
            username: "checkAvailabilityHandler"
        }
    })

    afterAll(async () => {
        await testHelper.deleteUser({ id: user.id })
    })

    test("normal execution", async () => {

        await userHandler.checkAvailabilityHandler(mockCtx, null)
        const { body: response } = mockCtx

        expect(response).toHaveProperty('username', "checkAvailabilityHandler")
        expect(response).toHaveProperty('isAvailable', false)

        testLogger.logRequestResponse(
            "Check username availability", mockCtx,
            "GET", "/user/availability"
        )
    })
})