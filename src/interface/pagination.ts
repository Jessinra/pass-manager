
export interface IPagination {

    first?: number,
    after?: string,
    last?: number,
    before?: string,
    skip?: number,
    orderBy?: string,

    page?: number,
}