
export interface IActiveUser {
    username: string,
    email?: string,
    iat?: string
}