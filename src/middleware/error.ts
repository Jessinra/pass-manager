
export async function errorHandler(ctx, next) {

    try {
        await next();
    }

    catch (err) {
        logError(ctx, err);
        ctx.body = err
    }
};

function logError(ctx: any, err: any) {
    
    console.log("\n\n============================");
    logTimestamp();
    logRequestUrl();
    logRequestBody();
    logErrorDetails();
    console.log("============================\n\n");

    function logTimestamp() {
        let timestamp = new Date().toISOString();
        console.log(timestamp);
    }

    function logErrorDetails() {
        console.log("\nError: ");
        console.log(err);
    }

    function logRequestBody() {
        console.log("\nRequest: ");
        console.log(ctx.request.body);
    }

    function logRequestUrl() {
        let method = ctx.request.method;
        let url = ctx.request.url;
        console.log(`${method} ${url}`);
    }
}