
import { IActiveUser } from "../interface/activeUser";
import ServiceError = require('../class/error')
import JWT = require("../util/JWT")


export async function authMiddleware(ctx, next): Promise<void> {

    var jwtToken: string
    var activeUser: IActiveUser

    const { authorization } = ctx.request.header;
    jwtToken = authorization.replace("Bearer ", "");
    activeUser = await authenticateUser(jwtToken)
    jwtToken = await refreshLoginStatus(activeUser, jwtToken);

    ctx.request.body.activeUser = { ...activeUser, jwtToken };
    await next();
    ctx.body.jwtToken = jwtToken
};

export async function publicMiddleware(ctx, next): Promise<void> {

    var jwtToken: string
    var activeUser: IActiveUser

    try {
        const { authorization } = ctx.request.header;
        jwtToken = authorization.replace("Bearer ", "");
        activeUser = await authenticateUser(jwtToken)
        jwtToken = await refreshLoginStatus(activeUser, jwtToken);
    }
    catch (err) {
        activeUser = getGuestUser()
        jwtToken = null
    }

    ctx.request.body.activeUser = { ...activeUser, jwtToken };
    await next();
    ctx.body.jwtToken = (ctx.body.jwtToken) ? ctx.body.jwtToken : jwtToken
};


async function authenticateUser(jwtToken: string): Promise<IActiveUser> {
    try {
        return await JWT.verifyJWT(jwtToken);
    }
    catch (err) {
        throw new ServiceError.FailedToAuthenticateUser(err.message);
    }
}

async function refreshLoginStatus(activeUser: IActiveUser, jwtToken: string): Promise<string> {
    try {
        const jwtCreatedAt = Number(activeUser.iat);
        if (await JWT.refreshRequired(jwtCreatedAt)) {
            jwtToken = await JWT.refreshJWT(activeUser);
        }
        return jwtToken
    }
    catch (err) {
        throw new ServiceError.FailedToAuthenticateUser("Failed Refresh JWT");
    }
}

function getGuestUser(): IActiveUser {
    return {
        username: "UnauthorizedUser",
        email: null,
    };
}